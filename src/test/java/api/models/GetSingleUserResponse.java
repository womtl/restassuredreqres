package api.models;

import lombok.Getter;

@Getter
public class GetSingleUserResponse {
    private UserData data;

    private Support support;
}
