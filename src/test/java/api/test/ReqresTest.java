package api.test;

import api.models.*;
import api.steps.ReqresSteps;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ReqresTest {

private final ReqresSteps reqresSteps = new ReqresSteps();

    @ParameterizedTest
    @ValueSource(strings = {"2"})
    public void successGetUser(String id) {
        GetSingleUserResponse respons = reqresSteps.getUser(id);

        Response response = reqresSteps.getUserResponse(id);

        reqresSteps.checkEmailSuccess(respons);
        reqresSteps.checkStatusCode(response,200);
    }

    @ParameterizedTest
    @ValueSource(strings = {"2"})
    public void unsuccessGetUser(String id) {
        GetSingleUserResponse respons = reqresSteps.getUser(id);

        reqresSteps.checkFirstnameUnsuccess(respons);
    }

    @Test
    public void successRegisterUser() {
        PostRegisterInput registers =  PostRegisterInput.builder().
                setEmail("eve.holt@reqres.in").
                setPassword("pistol").
                build();

        PostRegisterResponse respons = reqresSteps.postRegisterSuccess(registers);

        Response response = reqresSteps.postRegisterSuccessResponse(registers);

        reqresSteps.checkRegisterUserSuccess(respons);
        reqresSteps.checkStatusCode(response, 200);
    }

   @Test
    public void unsuccessRegisterUser() {
        PostRegisterInput registeru = PostRegisterInput.builder()
                .setEmail("sydney@fife")
                .build();

        PostRegisterResponse respons = reqresSteps.postRegisterUnsuccess(registeru);

        Response response = reqresSteps.postRegisterUnsuccessResponse(registeru);

        reqresSteps.checkRegisterUserUnsuccess(respons);
        reqresSteps.checkStatusCode(response, 400);
    }

    @ParameterizedTest
    @ValueSource(strings = {"2"})
    public void successUpdateUser(String id) {
        PutUpdateInput updates = PutUpdateInput.builder().
                setName("morpheus").
                setJob("zion resident")
                .build();

        PutUpdateResponse respons = reqresSteps.putUpdateSuccess(id, updates);

        Response response = reqresSteps.putUpdateSuccessResponse(id, updates);

        reqresSteps.checkUpdateUserSuccess(respons);
        reqresSteps.checkStatusCode(response,200);
    }

    @ParameterizedTest
    @ValueSource(strings = {"dfghfghfg"})
    public void unsuccessUpdateUser(String id) {
        PutUpdateInput updateu = PutUpdateInput.builder().
                setName("morpheus").
                setJob("zion resident").
                setTest("fghngvbh").
                build();

        Response response = reqresSteps.putUpdateUnsuccessResponse(id, updateu);

        reqresSteps.checkStatusCode(response,400);
    }

    @ParameterizedTest
    @ValueSource(strings = {"2"})
    public void successDeleteUser(String id) {
        Response response = reqresSteps.deleteUserSuccessResponse(id);

        reqresSteps.checkStatusCode(response,204);
    }

    @ParameterizedTest
    @ValueSource(strings = {"dfghfghfg"})
    public void unsuccessDeleteUser(String id) {
        Response response = reqresSteps.deleteUserUnsuccessResponse(id);

        reqresSteps.checkStatusCode(response,400);
    }
}
