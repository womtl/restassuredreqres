package api.steps;

import api.models.*;

import api.models.GetSingleUserResponse;
import api.models.PostRegisterResponse;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import static io.restassured.RestAssured.given;

public class ReqresSteps {
    public GetSingleUserResponse getUser(String id) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .get(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .as(GetSingleUserResponse.class);
    }

    public Response getUserResponse(String id) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .get(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }

    public PostRegisterResponse postRegisterSuccess(PostRegisterInput registers) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(registers)
                .post("/api/register")
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .as(PostRegisterResponse.class);
    }

    public Response postRegisterSuccessResponse(PostRegisterInput registers) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(registers)
                .post("/api/register")
                .then()
                .extract()
                .response();
    }

    public PostRegisterResponse postRegisterUnsuccess(PostRegisterInput registeru) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(registeru)
                .post("/api/register")
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .as(PostRegisterResponse.class);
    }

    public Response postRegisterUnsuccessResponse(PostRegisterInput registeru) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(registeru)
                .post("/api/register")
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }


    public PutUpdateResponse putUpdateSuccess(String id, PutUpdateInput updates) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(updates)
                .put(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .as(PutUpdateResponse.class);
    }

    public Response putUpdateSuccessResponse(String id, PutUpdateInput updates) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body(updates)
                .put(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }

    public Response putUpdateUnsuccessResponse(String id, PutUpdateInput updateu) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .put(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }

    public Response deleteUserSuccessResponse(String id) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body("")
                .delete(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }

    public Response deleteUserUnsuccessResponse(String id) {
        return given()
                .spec(SpecHelper.RequestSpec())
                .when()
                .body("")
                .delete(String.format("/api/users/%s", id))
                .then()
                .spec(SpecHelper.ResponseSpec())
                .extract()
                .response();
    }

    public void checkEmailSuccess(GetSingleUserResponse respons) {
        String mail = "janet.weaver@reqres.in";

        Assertions.assertEquals(mail,respons.getData().getEmail());
    }

    public void checkFirstnameUnsuccess(GetSingleUserResponse response) {
        String firstname = "Weaver";

        Assertions.assertEquals(firstname,response.getData().getFirstName());
    }

    public void checkRegisterUserSuccess(PostRegisterResponse respons) {
        Integer id = 4;

        String token = "QpwL5tke4Pnpja7X4";

        Assertions.assertEquals(id, respons.getId());
        Assertions.assertEquals(token, respons.getToken());
    }

    public void checkStatusCode(Response response, int httpstatus) {
        Assertions.assertEquals(httpstatus, response.getStatusCode());
    }

    public void checkRegisterUserUnsuccess(PostRegisterResponse respons) {
        String expectedUnsuccess = "Missing password";

        Assertions.assertEquals(expectedUnsuccess, respons.getError());
    }

    public void checkUpdateUserSuccess(PutUpdateResponse respons) {

        String name = "morpheus";

        String job = "zion resident";

        Assertions.assertEquals(name, respons.getName());
        Assertions.assertEquals(job, respons.getJob());
        Assertions.assertNotNull(respons.getUpdatedAt());
    }
}
