package api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PutUpdateResponse {
    private String name;

    private String job;

    private String test;

    private String updatedAt;
}
