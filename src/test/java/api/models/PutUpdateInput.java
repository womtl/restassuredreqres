package api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder(setterPrefix = "set")
public class PutUpdateInput {
    public String name;

    public String job;

    public String test;
}
